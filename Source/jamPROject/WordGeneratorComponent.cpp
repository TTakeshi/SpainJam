#include "WordGeneratorComponent.h"
#include "Math/UnrealMathUtility.h"

UWordGeneratorComponent::UWordGeneratorComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
    Alphabet = "abcdefghijklmnopqrstuvwxyz";
}


// Called when the game starts
void UWordGeneratorComponent::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void UWordGeneratorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

FString UWordGeneratorComponent::RandomWord(int32 length)
{
    FString ResultString;
    while(ResultString.Len() < length)
    {
        int32 CurrentCharIndex = FMath::RandRange(0, Alphabet.Len() - 1);
        ResultString += Alphabet[CurrentCharIndex];
    }
    return ResultString.ToUpper();
}