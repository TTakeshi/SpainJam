// Copyright Epic Games, Inc. All Rights Reserved.

#include "jamPROjectCharacter.h"

#include "jamPROjectGameMode.h"
#include "MoCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Components/TextRenderComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

AjamPROjectCharacter::AjamPROjectCharacter()
    : TargetWord("")
    , TemporalWord("")
    , KeysPressed(0)
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Create a camera boom attached to the root (capsule)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Rotation of the character should not affect rotation of boom
	CameraBoom->bDoCollisionTest = false;
	CameraBoom->TargetArmLength = 500.f;
	CameraBoom->SocketOffset = FVector(0.f,0.f,75.f);
	CameraBoom->SetRelativeRotation(FRotator(0.f,180.f,0.f));

	// Create a camera and attach to boom
	SideViewCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("SideViewCamera"));
	SideViewCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	SideViewCameraComponent->bUsePawnControlRotation = false; // We don't want the controller rotating the camera

	TextRender = CreateDefaultSubobject<UTextRenderComponent>(TEXT("TextRender"));
	TextRender->SetupAttachment(RootComponent);
	TextRender->SetTextRenderColor(FColor::Blue);

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Face in the direction we are moving..
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 10000.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->GravityScale = 2.f;
	GetCharacterMovement()->AirControl = 0.80f;
	GetCharacterMovement()->JumpZVelocity = 1000.f;
	GetCharacterMovement()->GroundFriction = 3.f;
	GetCharacterMovement()->MaxWalkSpeed = 600.f;
	GetCharacterMovement()->MaxFlySpeed = 600.f;

	TargetMochachoRef = nullptr;
	GameModeRef = nullptr;
}

//////////////////////////////////////////////////////////////////////////
// Input

void AjamPROjectCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAxis("MoveRight", this, &AjamPROjectCharacter::MoveRight);
}

void AjamPROjectCharacter::MoveRight(float Value)
{
	// add movement in that direction
	AddMovementInput(FVector(0.f,-1.f,0.f), Value);
}

void AjamPROjectCharacter::SaveTemporalWord(FString charPressed)
{
	if (TargetMochachoRef != nullptr && TargetMochachoRef->GetFollow() == false)
	{
		if (KeysPressed > TargetWord.Len() - 1 )
		{
			KeysPressed = 0;
			TemporalWord = "";
		}

		TemporalWord = TemporalWord.Append(charPressed);
		TextRender->SetText(TemporalWord);

		if (KeysPressed == TargetWord.Len() - 1)
		{
			if (WordComparation())
			{
				TargetMochachoRef->SetFollow(true);
				//GameModeRef->CurrentAlliesUnited++;
				OKWordDelegate.Broadcast();
			}
		}

		KeysPressed++;
	}
}

bool AjamPROjectCharacter::WordComparation()
{
    if(TemporalWord.Equals(TargetWord))
    {
        return true;
    }
	BadWordDelegate.Broadcast();
	return false;
}