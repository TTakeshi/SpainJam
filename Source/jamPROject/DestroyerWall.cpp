#include "DestroyerWall.h"
#include "Math/Vector.h"
#include "jamPROjectGameMode.h"

ADestroyerWall::ADestroyerWall()
{
	PrimaryActorTick.bCanEverTick = true;
    bIsStopped = true;
}

void ADestroyerWall::BeginPlay()
{
	Super::BeginPlay();
	GameMode = Cast<AjamPROjectGameMode>(GetWorld()->GetAuthGameMode());
    GameMode->SetDestroyerWall(this);

    WaveTime = GameMode->StartWaveWallTime;
    CurrentWaveTime = WaveTime;

    //Invisiable Actor to calculate the Wall's movement
    FVector LocationForCalculus(GetActorLocation().X, TargetLocation.Y, TargetLocation.Z);
    ActorAtSameZandY->SetActorLocation(LocationForCalculus);
}

void ADestroyerWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

    // Velocity = Distance / Time.
    if(!bIsStopped)
    {
        CurrentWaveTime = CurrentWaveTime - DeltaTime;

        float RemainingDistance = FVector::Dist(ActorAtSameZandY->GetActorLocation(), TargetLocation);
        float VelocityToApply = RemainingDistance * 0.01 / CurrentWaveTime; // * 0.01 to be Meters/Seconds
        
        FVector VelocityVector = GetActorForwardVector() * VelocityToApply;        
        FVector NewLocation = GetActorLocation() + VelocityVector;
        SetActorLocation(NewLocation);

        FVector VelocityVectorInvisibleActor = ActorAtSameZandY->GetActorForwardVector() * VelocityToApply; 
        FVector NewLocationInvisibleActor = ActorAtSameZandY->GetActorLocation() + VelocityVectorInvisibleActor;
        ActorAtSameZandY->SetActorLocation(NewLocationInvisibleActor);
    }
    else
    {
        CurrentWaveTime = WaveTime;
        //TODO: Explosion o algo.
    }
}

void ADestroyerWall::SetIsStopped(bool _bIsStopped)
{
    bIsStopped = _bIsStopped;
}
