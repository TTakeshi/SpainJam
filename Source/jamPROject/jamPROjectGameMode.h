// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "jamPROjectGameMode.generated.h"

class AMoSpawner;
class ADestroyerWall;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FWaveStartedDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FBetweenWavesDelegate);

UCLASS(minimalapi)
class AjamPROjectGameMode : public AGameModeBase
{
	GENERATED_BODY()

    TArray<AMoSpawner*> AllSpawners;
    TArray<AMoSpawner*> SelectedWaveSpawners;

    ADestroyerWall* DestroyerWall;

public:
    UPROPERTY(BlueprintReadWrite)
    int32 CurrentWave;

    UPROPERTY(BlueprintReadWrite)
    int32 CurrentAlliesUnited;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float StartWaveWallTime;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float TimeBetweenWaves;

    UPROPERTY(BlueprintReadWrite)
    int32 GameTotalScore;

    ////////////////////////
    /// TIMER HANDLERS  ///
    ///////////////////////
    FTimerHandle WaveTimerHandle;
    FTimerHandle BetwenWavesTimerHandle;

    ///////////////////
    /// DELEGATES  ///
    //////////////////
    UPROPERTY(BlueprintAssignable)
	FWaveStartedDelegate WaveStartedDelegate;

    UPROPERTY(BlueprintAssignable)
	FBetweenWavesDelegate BetweenWavesDelegate;

public:
	AjamPROjectGameMode();

    UFUNCTION(BlueprintCallable)
    void StartGame();

    UFUNCTION(BlueprintCallable)
    void StartNextWave();

    UFUNCTION(BlueprintCallable)
    void EndCurrentWave(bool bPlayerHasSurvived);

    UFUNCTION(BlueprintCallable)
    void EndGame();

    UFUNCTION(BlueprintCallable)
    void AddSpawner(AMoSpawner* MoSpawner);

    UFUNCTION(BlueprintCallable)
    void SelectRandomSpawners();

    UFUNCTION(BlueprintCallable)
    void SetDestroyerWall(ADestroyerWall* _DestroyerWall);

protected:
    void Initialization();

    UFUNCTION()
    void WaveTimerEnded();
};



