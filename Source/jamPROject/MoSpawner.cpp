#include "MoSpawner.h"
#include "jamPROjectGameMode.h"


AMoSpawner::AMoSpawner()
{
    MoCharacter = nullptr;
}

void AMoSpawner::BeginPlay()
{
	Super::BeginPlay();
	
    AjamPROjectGameMode* GameMode = Cast<AjamPROjectGameMode>(GetWorld()->GetAuthGameMode());
    GameMode->AddSpawner(this);
}

void AMoSpawner::SpawnMoCaracter()
{
    UClass* GeneratedClass = ActorToSpawn_BP.Get();
    const FVector SpawnLocation = GetActorLocation();
	const FRotator SpawnRotation = GetActorRotation();
    FActorSpawnParameters SpawnInfo;

    MoCharacter = GetWorld()->SpawnActor(GeneratedClass, &SpawnLocation, &SpawnRotation, SpawnInfo);
}

void AMoSpawner::DestroyMoCaracter()
{
    if(MoCharacter)
    {
        MoCharacter->Destroy();
        MoCharacter = nullptr;
    }
}