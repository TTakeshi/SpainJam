// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "JamProjectController.generated.h"

/**
 * 
 */
UCLASS()
class JAMPROJECT_API AJamProjectController : public APlayerController
{
    GENERATED_BODY()

protected:
    virtual void BeginPlay() override;
};
