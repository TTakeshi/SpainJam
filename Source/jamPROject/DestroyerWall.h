// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DestroyerWall.generated.h"

class AjamPROjectGameMode;

UCLASS()
class JAMPROJECT_API ADestroyerWall : public AActor
{
	GENERATED_BODY()

public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FVector TargetLocation;

    UPROPERTY(EditAnywhere)
    AActor* ActorAtSameZandY;

private:
    bool bIsStopped;

    float WaveTime;
    float CurrentWaveTime;

    AjamPROjectGameMode* GameMode;

public:	
    ADestroyerWall();

	virtual void Tick(float DeltaTime) override;

    UFUNCTION(BlueprintCallable)
    void SetIsStopped(bool _bIsStopped);

protected:
	virtual void BeginPlay() override;
};
