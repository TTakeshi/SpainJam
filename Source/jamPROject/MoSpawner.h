// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MoCharacter.h"
#include "MoSpawner.generated.h"

UCLASS()
class JAMPROJECT_API AMoSpawner : public AActor
{
	GENERATED_BODY()

    AActor* MoCharacter;

public:
    UPROPERTY(EditAnywhere)
    TSubclassOf<AActor> ActorToSpawn_BP;
	
public:	
	// Sets default values for this actor's properties
	AMoSpawner();

    UFUNCTION(BlueprintCallable)
    void SpawnMoCaracter();

    UFUNCTION(BlueprintCallable)
    void DestroyMoCaracter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

};
