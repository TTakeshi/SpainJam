// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MoCharacter.generated.h"

class UTextRenderComponent;
class UWordGeneratorComponent;

UCLASS()
class JAMPROJECT_API AMoCharacter : public ACharacter
{
	GENERATED_BODY()

    /** Word Generator Component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "JAM|WordGenerator", meta = (AllowPrivateAccess = "true"))
	UWordGeneratorComponent* WordGenerator;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "JAM|TextRender", meta = (AllowPrivateAccess = "true"))
	UTextRenderComponent* TextRender;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "JAM|WordLenght", meta = (AllowPrivateAccess = "true"))
		int32 WordLenght;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "JAM|TargetWord", meta = (AllowPrivateAccess = "true"))
		FString TargetWord;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "JAM|Follow", meta = (AllowPrivateAccess = "true"))
		bool Follow;

public:
	// Sets default values for this character's properties
	AMoCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable)
		FString GetWord();

    UFUNCTION(BlueprintCallable)
		void SetFollow(bool _Follow);
	UFUNCTION(BlueprintCallable)
    bool GetFollow();
};
