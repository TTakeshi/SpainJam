// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "jamPROjectCharacter.generated.h"

class AjamPROjectGameMode;
class UTextRenderComponent;
class AMoCharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FCorrectWordDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FIncorrectWordDelegate);

UCLASS(config=Game)
class AjamPROjectCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Side view camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* SideViewCameraComponent;

	/** Camera boom positioning the camera beside the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "JAM|TextRender", meta = (AllowPrivateAccess = "true"))
		UTextRenderComponent* TextRender;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "JAM|TargetMochacho", meta = (AllowPrivateAccess = "true"))
		AMoCharacter* TargetMochachoRef;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "JAM|GameMode", meta = (AllowPrivateAccess = "true"))
		AjamPROjectGameMode* GameModeRef;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "JAM|TemporalWord", meta = (AllowPrivateAccess = "true"))
		FString TemporalWord;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "JAM|TemporalWord", meta = (AllowPrivateAccess = "true"))
		uint8 KeysPressed;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "JAM|TemporalWord", meta = (AllowPrivateAccess = "true"))
		FString TargetWord;

	UPROPERTY(BlueprintAssignable)
		FCorrectWordDelegate OKWordDelegate;

    UPROPERTY(BlueprintAssignable)
		FIncorrectWordDelegate BadWordDelegate;


protected:

	/** Called for side to side input */
	//void MoveRight(float Val);

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface


public:
	AjamPROjectCharacter();

	/** Returns SideViewCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetSideViewCameraComponent() const { return SideViewCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	UFUNCTION(BlueprintCallable)
		void SaveTemporalWord(FString charPressed);

    UFUNCTION(BlueprintCallable)
		bool WordComparation();

    UFUNCTION(BlueprintCallable)
	void MoveRight(float Val);

};
