// Fill out your copyright notice in the Description page of Project Settings.


#include "MoCharacter.h"

#include "WordGeneratorComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/TextRenderComponent.h"
#include "jamPROjectGameMode.h"

// Sets default values
AMoCharacter::AMoCharacter()
    : WordLenght(0)
    , TargetWord("meh")
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TextRender = CreateDefaultSubobject<UTextRenderComponent>(TEXT("TextRender"));
	TextRender->SetupAttachment(RootComponent);
	TextRender->SetTextRenderColor(FColor::Yellow);

	WordGenerator = CreateDefaultSubobject<UWordGeneratorComponent>(TEXT("WordGenerator"));

}

// Called when the game starts or when spawned
void AMoCharacter::BeginPlay()
{
	Super::BeginPlay();

    AjamPROjectGameMode* GameMode = Cast<AjamPROjectGameMode>(GetWorld()->GetAuthGameMode());

    WordLenght = GameMode->CurrentWave + 1;

	TargetWord = WordGenerator->RandomWord(WordLenght);
	TextRender->SetText(TargetWord);
	
}

// Called every frame
void AMoCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMoCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

FString AMoCharacter::GetWord()
{
	return TargetWord;
}

void AMoCharacter::SetFollow(bool _Follow)
{
	Follow = _Follow;
}

bool AMoCharacter::GetFollow()
{
	return Follow;
}

