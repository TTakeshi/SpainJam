// Fill out your copyright notice in the Description page of Project Settings.


#include "JamProjectController.h"

#include "Framework/Application/NavigationConfig.h"

void AJamProjectController::BeginPlay()
{
	Super::BeginPlay();

	if (IsLocalPlayerController())
	{
        const TSharedRef<FNavigationConfig> Navigation = MakeShared<FNavigationConfig>();
	
		Navigation->bKeyNavigation = false;
		Navigation->bTabNavigation = false;
		Navigation->bAnalogNavigation = false;
		FSlateApplication::Get().SetNavigationConfig(Navigation);
	}
}
