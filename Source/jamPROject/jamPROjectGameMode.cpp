// Copyright Epic Games, Inc. All Rights Reserved.

#include "jamPROjectGameMode.h"
#include "jamPROjectCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Math/UnrealMathUtility.h"
#include "MoSpawner.h"
#include "DestroyerWall.h"

AjamPROjectGameMode::AjamPROjectGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/SideScrollerCPP/Blueprints/SideScrollerCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

    StartWaveWallTime = 60.0f;
    TimeBetweenWaves = 5.0f;
    AllSpawners = {};
    SelectedWaveSpawners = {};
}

void AjamPROjectGameMode::Initialization()
{
    CurrentWave = 1;
    CurrentAlliesUnited = 0;
    GameTotalScore = 0;
}

void AjamPROjectGameMode::StartGame()
{
    Initialization();
    StartNextWave();
}

void AjamPROjectGameMode::StartNextWave()
{
    SelectRandomSpawners();
    GetWorld()->GetTimerManager().SetTimer(WaveTimerHandle, this, &AjamPROjectGameMode::WaveTimerEnded, StartWaveWallTime, false);
    DestroyerWall->SetIsStopped(false);
    WaveStartedDelegate.Broadcast();

    for(AMoSpawner* Spawner : SelectedWaveSpawners)
    {
        Spawner->SpawnMoCaracter();
    }
}

void AjamPROjectGameMode::EndCurrentWave(bool bPlayerHasSurvived)
{
    GetWorld()->GetTimerManager().ClearTimer(WaveTimerHandle);
    GetWorld()->GetTimerManager().ClearTimer(BetwenWavesTimerHandle);

    for(AMoSpawner* Spawner : SelectedWaveSpawners)
    {
        Spawner->DestroyMoCaracter();
    }

    SelectedWaveSpawners.Empty();
    DestroyerWall->SetIsStopped(true);

    if(bPlayerHasSurvived)
    {
        GameTotalScore++;
        CurrentWave++;
        CurrentAlliesUnited = 0;
        BetweenWavesDelegate.Broadcast();
        GetWorld()->GetTimerManager().SetTimer(BetwenWavesTimerHandle, this, &AjamPROjectGameMode::StartNextWave, TimeBetweenWaves, false);
    }
    else
    {
        EndGame();
    }
}

void AjamPROjectGameMode::EndGame()
{
    //End game stuff
}

void AjamPROjectGameMode::AddSpawner(AMoSpawner* MoSpawner)
{
    AllSpawners.Add(MoSpawner);
}

void AjamPROjectGameMode::SelectRandomSpawners()
{
    TArray<AMoSpawner*> AllSpawnersCopy(AllSpawners);

    if(CurrentWave < AllSpawners.Num())
    {
        for(int i = 0; i < CurrentWave; ++i)
        {
            int32 ChosenIndex = FMath::RandRange(0, AllSpawnersCopy.Num() - 1);
            SelectedWaveSpawners.Add(AllSpawnersCopy[ChosenIndex]);

            AllSpawnersCopy.RemoveAt(ChosenIndex);
        }
    }
    else
    {
        SelectedWaveSpawners = AllSpawners;
    }
}

void AjamPROjectGameMode::SetDestroyerWall(ADestroyerWall* _DestroyerWall)
{
    DestroyerWall = _DestroyerWall;
}

void AjamPROjectGameMode::WaveTimerEnded()
{
    EndCurrentWave(true);
}